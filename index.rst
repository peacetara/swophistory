.. SWOP-USA History project, created by
   sphinx-quickstart on Sun Dec 16 12:45:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SWOP-USA History project!
==================================================

Contents:

.. toctree::
   :maxdepth: 2
   :glob:
   
   *



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Sex Workers Outreach Project History
====================================

* Built Pages available at: https://peacetara.gitlab.io/swophistory
* Source of the book available at: https://gitlab.com/peacetara/swophistory
