# swophistory

History book of SWOP-USA (www.swopusa.org)


* Built Pages available at: https://peacetara.gitlab.io/swophistory
* Source of the book available at: https://gitlab.com/peacetara/swophistory

This project uses reStructuredText.  a quick reference is here: http://docutils.sourceforge.net/docs/user/rst/quickref.html

Also, You can play with reStructuredText and see live preview and stuff here:
http://rst.ninjs.org/

if reStructuredText is to much for you, just write it all in plain text, commit it here and make tara fix it.