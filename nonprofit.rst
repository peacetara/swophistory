Non Profit
==========

Tara Sawyer did the initial paperwork and applied in California as a non-profit for SWOP on December 5, 2007.  California granted us incorporation on March 3, 2008.

IRS initially denied our application on May 15, 2011, but after an appeal, IRS Granted us 501c(3) status on July 12, 2013 (and made it retroactive to our 2008 creation).
